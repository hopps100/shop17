@extends('layouts.app')

@section('content')
     <h1>Familias</h1>

    <table class='table table-striped'>
        <thead>
            <tr>
                <th>Id</th>
                <th>Código</th>
                <th>Nombre</th>
                <th>Opciones</th>
            </tr>
        </thead>
        <tbody id="tbodyMain">
            <tr>
                <td>{{ $family['id'] }}</td>
                <td>{{ $family['code'] }}</td>
                <td>{{ $family['name'] }}</td>
                <td>
                    <a href="/family/update/{{$family->id }}">Editar</a>
                    <a href="/family/delete/{{$family->id }}">Borrar</a>
                </td>
            </tr>
        </tbody>
    </table> 

    <script>
    /*var objeto_json = JSON.parse(jsonResponse);
    var familias */


    /*$.ajax({

        ulr: 'localhost:8000/famili/index',
        type: "get" ,
        sucess: function (response){

        }
    })*/


    /*$(documen).ready(function ()
    {
        var loadData = function () 
        {
            url = '/family';
            $.get(url, "" , function (data)
            {
                $('#tbodyMain').empty();

                for (var i=0; i<data.length; i++)
                {
                    var newRow = '<tr id="row'+ data[i].id + '" </tr>';
                    newRow = newRow + '<td> id </td>'
                    newRow = newRow + '<td> code </td>'
                    newRow = newRow + '<td> name </td>'
                }
            })
        }
    })*/
    </script>    
    @yield('scripts') 
@stop

