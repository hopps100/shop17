@extends('layouts.app')

@section('content')
     <h1>Destalles de familia</h1>

    <table class='table table-striped'>
        <thead>
            <tr>
                <th>Id</th>
                <th>Código</th>
                <th>Nombre</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $family['id'] }}</td>
                <td>{{ $family['code'] }}</td>
                <td>{{ $family['name'] }}</td>
            </tr>
        </tbody>
    </table>      
@stop

