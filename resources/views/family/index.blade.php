@extends('layouts.app')

@section('content')
     <h1>Lista de familias</h1><a href="/families/create/">Nueva familia</a>

    <table class='table table-striped'>
        <thead>
            <tr>
                <th>Id</th>
                <th>Código</th>
                <th>Nombre</th>
                <th>Opciones</th>
            </tr>
        </thead>
        <tbody>
        @foreach($families as $family)
            <tr>
                <td>{{ $family['id'] }}</td>
                <td>{{ $family['code'] }}</td>
                <td>{{ $family['name'] }}</td>
                <td>
                    <a href="/families/{{$family->id }}/edit/">Editar</a>
                    <form method="post" action="/families/{{$family->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" value="Borrar">
                    </form>
                    <a href="/families/{{$family->id }}">Ver</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>      
@stop

