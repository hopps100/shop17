@extends('layouts.app')

@section('content')
    <h1>Alta de estudios</h1>
    <p>
        Código: {{ $family->code }} <b>
        Nombre: {{ $family->name }}
    </p>

    <form  action="/families/" method="post">
         {{ csrf_field() }}
         <ipnut type="hidden" name="_method" value="PUT">
         <input type="hidden" name="id" value="{{ $family['id'] }}"><br>

         <div class= "form-group"> 
         <label>Código</label>
         <input type="text" name="code" value="{{ $family['code'] }}">

        </div>

        <div class= "form-group">
         <label>Nombre</label>
         <input type="text" name="name" value="{{ $family['name'] }}">
        </div>

         
         <label>Enviar</label>
         <input type="submit" value="Enviar"><br>
    </form>

@stop